const admin = require('firebase-admin');
admin.initializeApp();
const functions = require('firebase-functions')
const firebase = require("firebase");
const jsSHA = require("jssha");
const request = require("request");
const nodemailer = require('nodemailer');
const cors = require('cors')({ origin: true });
// Required for side-effects
require("firebase/firestore");
// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
  apiKey: "AIzaSyC9y0-tGrHO8jkt7lUk5_yJhZWt3lw73CU",
  authDomain: "luteraa-esports.firebaseapp.com",
  projectId: "luteraa-esports",
  storageBucket: "luteraa-esports.appspot.com",
  messagingSenderId: "262294088700",
  appId: "1:262294088700:web:22f847e5b31498e4ef6079",
  measurementId: "G-1RL84NZNKE"

});

let transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'luteraaofficial@gmail.com',
    pass: 'Alpha+bta@340'
  }
});

exports.createUser = functions.firestore
  .document('users/{userId}')
  .onCreate((snap, context) => {
    // Get an object representing the document
    // e.g. {'name': 'Marie', 'age': 66}referralCode
    const newValue = snap.data();

    // perform desired operations ...
    const mailOptions = {
      from: 'luteraaofficial@gmail.com', // Something like: Jane Doe <janedoe@gmail.com>
      to: 'sibaprasad1029@gmail.com',
      subject: 'User sign up alert', // email subject
      html: '<p style="font-size: 16px;">A new user has sign up</p><br/><p>Email: ' + newValue.email + '</p><br/><p>Name: ' + newValue.name + '</p><br/><p>Phone: ' + newValue.phone + '</p><br/><p>Referral Code: ' + newValue.referralCode + '</p>'
    };

    // returning result
    return transporter.sendMail(mailOptions, (erro, info) => {
      if (erro) {
        console.log(erro);
        return erro;
      }
      return 'sent';
    });
  });


exports.sendAlertToUser = functions.https.onCall((data, context) => {
  const regId = data.regId;
  const messagebody = data.messagebody;
  const title = data.title;
  const id = data.id;
  const docId = data.docId;
  const username = data.username;
  const type = data.type;
  console.log(regId + "\n" + messagebody + "-" + title + "-" + id + "-" + username);

  var message = {
    notification: {
      title: title,
      body: messagebody,
    },
    data:
    {
      type: type,
      username: username,
      docId: docId,
      id: id,
      click_action: "FLUTTER_NOTIFICATION_CLICK"
    },
    token: regId
  };
  admin.messaging().send(message);
  return { message: 'Done' }
});


// Take the text parameter passed to this HTTP endpoint and insert it into 
// Firestore under the path /messages/:documentId/original
exports.success = functions.https.onRequest(async (req, res) => {
  // Grab the text parameter.
  const userID = req.query.userID;
  const amount = req.query.amount;
  const trxid = req.query.trxid;
  // Push the new message into Firestore using the Firebase Admin SDK.
  const writeResult = await admin.firestore().collection('messages').add({ userID: userID, amount: amount, trxid: trxid });
  // Send back a message that we've successfully written the message
  res.json({ result: `Message with ID: ${writeResult.id} added.` });
});

// Take the text parameter passed to this HTTP endpoint and insert it into 
// Firestore under the path /messages/:documentId/original
exports.fail = functions.https.onRequest(async (req, res) => {
  // Grab the text parameter.
  const userID = req.query.userID;
  const amount = req.query.amount;
  const trxid = req.query.trxid;
  // Push the new message into Firestore using the Firebase Admin SDK.
  const writeResult = await admin.firestore().collection('messages').add({ userID: userID, amount: amount, trxid: trxid });
  // Send back a message that we've successfully written the message
  res.json({ result: `Message with ID: ${writeResult.id} added.` });
});



// Take the text parameter passed to this HTTP endpoint and insert it into 
// Firestore under the path /messages/:documentId/original
exports.generateHash = functions.https.onCall((pay, context) => {
  console.log(pay.toString());
  const hashString = 'guAq4r' //store in in different file gtkFFx key
    + '|' + pay.txnid
    + '|' + pay.amount
    + '|' + pay.productname
    + '|' + pay.name
    + '|' + pay.email
    + '|||||||||||'
    + 'B1Kk4BVD'; //store in in different file salt B1Kk4BVD eCwWELxi
    console.log(hashString);
  const sha = new jsSHA('SHA-512', "TEXT");
  sha.update(hashString);
  //Getting hashed value from sha module
  const hash = sha.getHash("HEX");
  return({
    "status" : 'success',
    "data" : hash
    });
});
