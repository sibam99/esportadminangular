import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmallbannersitemComponent } from './smallbannersitem.component';

describe('SmallbannersitemComponent', () => {
  let component: SmallbannersitemComponent;
  let fixture: ComponentFixture<SmallbannersitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmallbannersitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmallbannersitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
