import { Component, OnInit, Input } from '@angular/core';
import { Smallbanners } from '../smallbanners';
import { AppService } from 'src/app/shared/services/app.service'; import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import * as moment from 'moment';
import { ConfirmModalComponent } from '../../confirm-dialog/confirm-dialog.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'tr[app-smallbannersitem]',
  templateUrl: './smallbannersitem.component.html',
  styleUrls: ['./smallbannersitem.component.css']
})
export class SmallbannersitemComponent implements OnInit {

  @Input() item: Smallbanners;
  @Input() i: number;
  editing: boolean = false;
  notediting: boolean = true;
  temp: Smallbanners;
  modalRef: BsModalRef;
  constructor(private service: AppService, private snackBar: MatSnackBar, private modalService: BsModalService, private storage: AngularFireStorage) { }

  ngOnInit(): void {
  }
  initiateEdit() {
    this.editing = true;
    this.notediting = false;
    this.temp = JSON.parse(JSON.stringify(this.item));
  }
  commitEdit() {
    if (this.item.name == null || this.item.name.length == 0) {
      this.openSnackBar("Please add Name.");
      return;
    }
    if (this.item.imagelink == null || this.item.imagelink.length == 0) {
      this.openSnackBar("Please add Imagelink.");
      return;
    }
    this.editing = false;
    this.notediting = true;
    this.service.updateSmallbanners(this.item);
    this.openSnackBar("Update success");
  }
  cancelEdit() {
    this.editing = false;
    this.notediting = true;
    this.item = this.temp;
    this.ngOnInit();
  }
  openSnackBar(x) {
    this.snackBar.open(x, 'Dismiss', {
      duration: 2000
    })
  }

  deleteRecord() {
    this.modalRef = this.modalService.show(ConfirmModalComponent, {
      initialState: {
        prompt: 'Are you sure you want to delete this record?',
        callback: (result) => {
          if (result == 'yes') {
            this.service.deleteItem(this.item.id, 'smallbannerses');
          }
        }
      }
    });
  }
}
