import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmallbannersComponent } from './smallbanners.component';

describe('SmallbannersComponent', () => {
  let component: SmallbannersComponent;
  let fixture: ComponentFixture<SmallbannersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmallbannersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmallbannersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
