import { Component, OnInit } from '@angular/core';
import { Smallbanners } from './smallbanners';
import { AngularFirestore } from '@angular/fire/firestore';
import { AppService } from 'src/app/shared/services/app.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';
import { FormControl } from '@angular/forms';
import * as moment from 'moment';
import { map, startWith, finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AngularFireStorageReference, AngularFireUploadTask, AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-smallbanners',
  templateUrl: './smallbanners.component.html',
  styleUrls: ['./smallbanners.component.css']
})
export class SmallbannersComponent implements OnInit {
  creating: boolean;
  selitem: Smallbanners;
  showList: boolean = true;
  tableData: any;
  loading = true;
  count: number;
  constructor(private firestore: AngularFirestore, private storage: AngularFireStorage, private service: AppService, private snackBar: MatSnackBar, private titleService: Title) { }

  ngOnInit(): void {
    this.setTitle('Small Banners')
    this.loadItems();
    this.loadAllCounts();
  }
  //Save first document in snapshot of items received
  firstInResponse: any = [];

  //Save last document in snapshot of items received
  lastInResponse: any = [];

  //Keep the array of first document of previous pages
  prev_strt_at: any = [];

  //Maintain the count of clicks on Next Prev button
  pagination_clicked_count = 0;

  //Disable next and prev buttons
  disable_next: boolean = true;
  disable_prev: boolean = false;
  limit: number = 20;
  orderBy: string = 'createdat';
  dbPath: string = 'smallbannerses';
  onCreate() {
    console.log('Creatting');
    this.selitem = new Smallbanners();
    this.showList = false;
    this.creating = true;
  }

  loadAllCounts() {
    this.firestore.collection(this.dbPath).snapshotChanges()
      .subscribe(response => {
        if (!response.length) {
          this.count = 0;
          this.dismissSpinner();
        }
        else if (response.length < this.limit) {
          this.count = 1;
        }
        else {
          this.count = Math.ceil(response.length / this.limit);
        }
        this.disable_next = (this.pagination_clicked_count + 1) == this.count;
      }, error => {
      });

  }

  loadItems() {
    this.firestore.collection(this.dbPath, ref => ref
      .limit(this.limit)
      .orderBy(this.orderBy, 'desc')
    ).snapshotChanges()
      .subscribe(response => {
        this.tableData = [];
        if (!response.length) {
          console.log("No Data Available");
          return false;
        }
        this.firstInResponse = response[0].payload.doc;
        this.lastInResponse = response[response.length - 1].payload.doc;

        for (let e of response) {
          this.tableData.push({
            id: e.payload.doc.id,
            ...e.payload.doc.data() as Smallbanners
          });

        }

        //Initialize values
        this.prev_strt_at = [];
        this.pagination_clicked_count = 0;
        this.disable_next = (this.pagination_clicked_count + 1) == this.count;
        this.disable_prev = false;

        //Push first item to use for Previous action
        this.push_prev_startAt(this.firstInResponse);
        this.loading = false;
      }, error => {
      });
  }
  // Add item in Collection
  addItem() {
    if (this.selitem.name == null || this.selitem.name.length == 0) {
      this.openSnackBar("Please add Name.");
      return;
    }
    if (this.selitem.imagelink == null || this.selitem.imagelink.length == 0) {
      this.openSnackBar("Please add Image.");
      return;
    }
    this.service.addSmallbanners(this.selitem);
    this.openSnackBar("Record Added");
    this.creating = false;
    this.showList = true;
  }

  //Show previous set 
  prevPage() {
    this.showSpinner()
    this.disable_prev = true;
    this.firestore.collection(this.dbPath, ref => ref
      .orderBy(this.orderBy, 'desc')
      .startAt(this.get_prev_startAt())
      .endBefore(this.firstInResponse)
      .limit(this.limit)
    ).get()
      .subscribe(response => {
        this.firstInResponse = response.docs[0];
        this.lastInResponse = response.docs[response.docs.length - 1];

        this.tableData = [];
        for (let e of response.docs) {
          this.tableData.push({
            id: e.id,
            ...e.data() as Smallbanners
          });
        }

        //Maintaing page no.
        this.pagination_clicked_count--;

        //Pop not required value in array
        this.pop_prev_startAt(this.firstInResponse);

        //Enable buttons again
        this.disable_prev = false;
        this.disable_next = (this.pagination_clicked_count + 1) == this.count;
        this.disable_prev = false;
        this.dismissSpinner();
      });
  }
  showSpinner() {
    this.loading = true
  }
  dismissSpinner() {
    this.loading = false
  }
  nextPage() {
    this.showSpinner();
    this.disable_next = true;
    this.firestore.collection(this.dbPath, ref => ref
      .limit(this.limit)
      .orderBy(this.orderBy, 'desc')
      .startAfter(this.lastInResponse)
    ).get()
      .subscribe(response => {

        if (!response.docs.length) {
          this.disable_next = true;
          return;
        }

        this.firstInResponse = response.docs[0];

        this.lastInResponse = response.docs[response.docs.length - 1];
        this.tableData = [];
        for (let e of response.docs) {
          this.tableData.push({
            id: e.id,
            ...e.data() as Smallbanners
          });
        }


        this.pagination_clicked_count++;

        this.push_prev_startAt(this.firstInResponse);

        this.disable_next = (this.pagination_clicked_count + 1) == this.count;
        this.dismissSpinner();
      }, error => {
        this.dismissSpinner();
        this.disable_next = false;
      });
  }

  //Add document
  push_prev_startAt(prev_first_doc) {
    this.prev_strt_at.push(prev_first_doc);
  }

  //Remove not required document 
  pop_prev_startAt(prev_first_doc) {
    this.prev_strt_at.forEach(element => {
      if (prev_first_doc.data().id == element.data().id) {
        element = null;
      }
    });
  }

  //Return the Doc rem where previous page will startAt
  get_prev_startAt() {
    if (this.prev_strt_at.length > (this.pagination_clicked_count + 1))
      this.prev_strt_at.splice(this.prev_strt_at.length - 2, this.prev_strt_at.length - 1);
    return this.prev_strt_at[this.pagination_clicked_count - 1];
  }
  cancelCreate() {
    this.creating = false;
    this.showList = true;
  }

  openSnackBar(x) {
    this.dismissSpinner();
    this.snackBar.open(x, 'Dismiss', {
      duration: 2000
    })
  }
  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  uploading: boolean;

  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  uploadState: Observable<string>;
  uploadProgress: Observable<number>;
  downloadURL: Observable<string>;

  onFileChanged(event) {
    this.uploading = true;
    var n = this.service.randomString(40);
    const file = event.target.files[0];
    const filePath = `images/smallbanners/${n}`;
    const fileRef = this.storage.ref(filePath);
    this.task = this.storage.upload(`images/smallbanners/${n}`, file);
    this.uploadProgress = this.task.percentageChanges();
    this.task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.subscribe(url => {
            if (url) {
              this.selitem.imagelink = url;
              this.uploading = false
            }
          });
        })
      )
      .subscribe(url => {
        if (url) {
          console.log(url);
        }
      });
  }

}
