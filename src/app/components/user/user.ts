export class User {
    id: string;
    createdat: any
    name: string
    email: string
    gender: string
    phone: string
    approved
    role: string
    uid: string
    photoUrl: string
    emailVerified: string
    lastseen
    appversion
    model
    platform
    refferals
    userId
    cashcoins
}
