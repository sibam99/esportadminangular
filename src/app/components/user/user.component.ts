import { Component, OnInit } from '@angular/core';
import { User } from './user';
import { AngularFirestore } from '@angular/fire/firestore';
import { AppService } from 'src/app/shared/services/app.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  creating: boolean;
  selitem: User;
  showList: boolean = true;
  tableData: any;
  loading = true;
  count: number;
  constructor(private firestore: AngularFirestore, private route: ActivatedRoute, public router: Router, private service: AppService, private snackBar: MatSnackBar, private titleService: Title) { }

  ngOnInit(): void {
    this.setTitle('App Users')
    this.loadItems();
    this.loadAllCounts();
    this.loadAllusers();
  }
  //Save first document in snapshot of items received
  firstInResponse: any = [];

  //Save last document in snapshot of items received
  lastInResponse: any = [];

  //Keep the array of first document of previous pages
  prev_strt_at: any = [];

  //Maintain the count of clicks on Next Prev button
  pagination_clicked_count = 0;

  //Disable next and prev buttons
  disable_next: boolean = true;
  disable_prev: boolean = false;
  limit: number = 600;
  orderBy: string = 'email';
  dbPath: string = 'users';
  onCreate() {
    console.log('Creatting');
    this.selitem = new User();
    this.showList = false;
    this.creating = true;
  }

  setOrderBy(orderBy: string) {
    this.orderBy = orderBy;
    this.loadItems();
  }

  loadAllCounts() {
    this.firestore.collection(this.dbPath).snapshotChanges()
      .subscribe(response => {
        if (!response.length) {
          this.count = 0;
          this.dismissSpinner();
        }
        else if (response.length < this.limit) {
          this.count = 1;
        }
        else {
          this.count = Math.ceil(response.length / this.limit);
        }
        this.disable_next = (this.pagination_clicked_count + 1) == this.count;
      }, error => {
      });

  }

  loadItems() {
    this.firestore.collection(this.dbPath, ref => ref
      .limit(this.limit)
      .orderBy(this.orderBy, 'desc')
    ).snapshotChanges()
      .subscribe(response => {
        if (!response.length) {
          console.log("No Data Available");
          return false;
        }
        this.firstInResponse = response[0].payload.doc;
        this.lastInResponse = response[response.length - 1].payload.doc;

        this.tableData = [];
        for (let e of response) {
          this.tableData.push({
            id: e.payload.doc.id,
            ...e.payload.doc.data() as User
          });

        }

        //Initialize values
        this.prev_strt_at = [];
        this.pagination_clicked_count = 0;
        this.disable_next = (this.pagination_clicked_count + 1) == this.count;
        this.disable_prev = false;

        //Push first item to use for Previous action
        this.push_prev_startAt(this.firstInResponse);
        this.loading = false;
      }, error => {
      });
  }
  // Add item in Collection
  addItem() {
    if (this.selitem.name == null) {
      this.openSnackBar("Please add User Name.");
      return;
    }
    if (this.selitem.email == null) {
      this.openSnackBar("Please add Email.");
      return;
    }
    if (this.selitem.gender == null) {
      this.openSnackBar("Please add Gender.");
      return;
    }
    if (this.selitem.role == null) {
      this.openSnackBar("Please add Role.");
      return;
    }
    this.service.addUser(this.selitem);
    this.openSnackBar("Record Added");
    this.creating = false;
    this.showList = true;
  }

  //Show previous set 
  prevPage() {
    this.showSpinner()
    this.disable_prev = true;
    this.firestore.collection(this.dbPath, ref => ref
      .orderBy(this.orderBy, 'desc')
      .startAt(this.get_prev_startAt())
      .endBefore(this.firstInResponse)
      .limit(this.limit)
    ).get()
      .subscribe(response => {
        this.firstInResponse = response.docs[0];
        this.lastInResponse = response.docs[response.docs.length - 1];

        this.tableData = [];
        for (let e of response.docs) {
          this.tableData.push({
            id: e.id,
            ...e.data() as User
          });
        }

        //Maintaing page no.
        this.pagination_clicked_count--;

        //Pop not required value in array
        this.pop_prev_startAt(this.firstInResponse);

        //Enable buttons again
        this.disable_prev = false;
        this.disable_next = (this.pagination_clicked_count + 1) == this.count;
        this.disable_prev = false;
        this.dismissSpinner();
      });
  }
  showSpinner() {
    this.loading = true
  }
  dismissSpinner() {
    this.loading = false
  }
  nextPage() {
    this.showSpinner();
    this.disable_next = true;
    this.firestore.collection(this.dbPath, ref => ref
      .limit(this.limit)
      .orderBy(this.orderBy, 'desc')
      .startAfter(this.lastInResponse)
    ).get()
      .subscribe(response => {

        if (!response.docs.length) {
          this.disable_next = true;
          return;
        }

        this.firstInResponse = response.docs[0];

        this.lastInResponse = response.docs[response.docs.length - 1];
        this.tableData = [];
        for (let e of response.docs) {
          this.tableData.push({
            id: e.id,
            ...e.data() as User
          });
        }


        this.pagination_clicked_count++;

        this.push_prev_startAt(this.firstInResponse);

        this.disable_next = (this.pagination_clicked_count + 1) == this.count;
        this.dismissSpinner();
      }, error => {
        this.dismissSpinner();
        this.disable_next = false;
      });
  }

  //Add document
  push_prev_startAt(prev_first_doc) {
    this.prev_strt_at.push(prev_first_doc);
  }

  //Remove not required document 
  pop_prev_startAt(prev_first_doc) {
    this.prev_strt_at.forEach(element => {
      if (prev_first_doc.data().id == element.data().id) {
        element = null;
      }
    });
  }

  //Return the Doc rem where previous page will startAt
  get_prev_startAt() {
    if (this.prev_strt_at.length > (this.pagination_clicked_count + 1))
      this.prev_strt_at.splice(this.prev_strt_at.length - 2, this.prev_strt_at.length - 1);
    return this.prev_strt_at[this.pagination_clicked_count - 1];
  }
  cancelCreate() {
    this.creating = false;
    this.showList = true;
  }

  openSnackBar(x) {
    this.dismissSpinner();
    this.snackBar.open(x, 'Dismiss', {
      duration: 2000
    })
  }
  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }


  //code for adding a filter for driver.
  user: User;
  users: User[];
  filteredusers: Observable<User[]>;
  loadinguser = false;
  searchuser = new FormControl('');
  loadAllusers() {
    this.loadinguser = true;
    this.service.getAllUsers().subscribe(data => {
      this.users = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data() as User
        };
      })


      this.loadinguser = false;
      this.filteredusers = this.searchuser.valueChanges
        .pipe(
          startWith(''),
          map(value => typeof value === 'string' ? value : value.name),
          map(name => name ? this._filteruser(name) : this.users.slice())
        );
    });
  }

  displayFnuser(user: User): string {
    return user && user.email ? user.email : '';
  }

  private _filteruser(name: string): User[] {
    const filterValue = name.toLowerCase();
    return this.users.filter(option => option.email.toLowerCase().indexOf(filterValue) === 0);
  }
  optionSelecteduser(b: User) {
    this.router.navigate(['/profile/' + b.id], { relativeTo: this.route });
  }

}
