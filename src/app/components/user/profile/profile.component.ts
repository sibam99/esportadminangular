import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AppService } from 'src/app/shared/services/app.service';
import { User } from '../user';
import * as firebase from 'firebase';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  item: User;
  topics = [];
  topicscount: number;
  tags = [];
  addTransaction = false;
  tagsscount: number;
  videos: any;
  amount = 0
  cashcoins = 0
  videoscount: number;
  tableData: any[];
  constructor(private route: ActivatedRoute, private firestore: AngularFirestore, private service: AppService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.getItem(params.id);
      this.loadHistory(params.id);
      this.loadCashCoins(params.id)
    });
  }
  loadHistory(id) {
    this.firestore.collection('users').doc(id).collection("wallethistory", ref => ref.orderBy("date", 'desc').limit(30)).snapshotChanges()
      .subscribe(response => {
        if (!response.length) {
          console.log("No Data Available");
          return false;
        }
        this.tableData = [];
        for (let e of response) {
          this.tableData.push({
            id: e.payload.doc.id,
            ...e.payload.doc.data() as Object
          });

        }
      }, error => {
      });
  }

  openSnackBar(x) {
    this.snackBar.open(x, 'Dismiss', {
      duration: 2000
    })
  }
  addTransactionFormShow() {
    this.addTransaction = true;
  }
  cancelCreate() {
    this.addTransaction = false;
  }
  commitTransaction() {
    if (this.amount == 0) {
      this.openSnackBar("No prize amount added.");
      return;
    }
    this.firestore.collection('wallets').doc(this.item.id).update({ 'cashCoin': firebase.firestore.FieldValue.increment(this.amount) })
      .then(() => {
        this.firestore.collection('users').doc(this.item.id).collection("wallethistory").add({ open: this.cashcoins - this.amount, newvalue: this.cashcoins, prizeamount: this.amount, date: firebase.firestore.FieldValue.serverTimestamp(), narration: 'prize credit' })
        this.addTransaction = false;
        this.amount = 0;
        this.openSnackBar("user prize added."); 
      }) 
  }
  approve() {
    this.firestore.collection('users').doc(this.item.id).update({ 'approved': 'Approved', dateapproved: firebase.firestore.FieldValue.serverTimestamp() })
  }
  freeze() {
    this.firestore.collection('users').doc(this.item.id).update({ 'approved': false, dateapproved: firebase.firestore.FieldValue.serverTimestamp() })
  }

  getItem(id: string) {
    this.firestore.collection('users').doc(id).snapshotChanges().subscribe((item) => {
      this.item = {
        id: item.payload.id, ...item.payload.data() as User
      }
    });
  }  

  loadCashCoins(id) { 
    this.firestore.collection('wallets').doc(id).snapshotChanges().subscribe((value) => {
      this.cashcoins = value.payload.data()["cashCoin"] 
      console.log(this.cashcoins);
      
    })
  }

}
