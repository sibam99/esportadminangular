import { Component, OnInit, Input } from '@angular/core';
import { User } from '../user';
import { AppService } from 'src/app/shared/services/app.service'; import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { ConfirmModalComponent } from '../../confirm-dialog/confirm-dialog.component';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'tr[app-useritem]',
  templateUrl: './useritem.component.html',
  styleUrls: ['./useritem.component.css']
})
export class UseritemComponent implements OnInit {

  @Input() item: User;
  @Input() i: number;
  editing: boolean = false;
  notediting: boolean = true;
  temp: User;
  list: Observable<Object>;
  topics: any;
  topicscount: number;
  tags: any;
  tagsscount: number;
  videos: any;
  videoscount: number;
  modalRef: BsModalRef;
  constructor(private service: AppService,private firestore: AngularFirestore, private snackBar: MatSnackBar, private modalService: BsModalService) { }

  ngOnInit(): void {
  }

  initiateEdit() {
    this.editing = true;
    this.notediting = false;
    this.temp = JSON.parse(JSON.stringify(this.item));
  }


  async getNoOfTags(): Promise<number> {
    return 0;

  }
  getNoOfTopics(): number {
    return 10;
  }
  commitEdit() {
    if (this.item.name == null) {
      this.openSnackBar("Please add Firstname.");
      return;
    }
    if (this.item.email == null) {
      this.openSnackBar("Please add Email.");
      return;
    }
    if (this.item.gender == null) {
      this.openSnackBar("Please add Gender.");
      return;
    }
    if (this.item.role == null) {
      this.openSnackBar("Please add Role.");
      return;
    }
    if (this.item.emailVerified == null) {
      this.openSnackBar("Please add EmailVerified.");
      return;
    }
    this.editing = false;
    this.notediting = true;
    this.service.updateUser(this.item);
    this.openSnackBar("Update success");
  }
  cancelEdit() {
    this.editing = false;
    this.notediting = true;
    this.item = this.temp;
    this.ngOnInit();
  }
  openSnackBar(x) {
    this.snackBar.open(x, 'Dismiss', {
      duration: 2000
    })
  }

  deleteRecord() {
    this.modalRef = this.modalService.show(ConfirmModalComponent, {
      initialState: {
        prompt: 'Are you sure you want to delete this record?',
        callback: (result) => {
          if (result == 'yes') {
            this.service.deleteItem(this.item.id,'users').then(()=>{
              return;
            });
          }
        }
      }
    });
  }

}
