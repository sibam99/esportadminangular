import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatformitemComponent } from './platformitem.component';

describe('PlatformitemComponent', () => {
  let component: PlatformitemComponent;
  let fixture: ComponentFixture<PlatformitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatformitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatformitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
