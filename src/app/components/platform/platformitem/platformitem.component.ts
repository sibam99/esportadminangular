import { Component, OnInit, Input } from '@angular/core';
import { Platform } from '../platform';
import { AppService } from 'src/app/shared/services/app.service';import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'tr[app-platformitem]',
  templateUrl: './platformitem.component.html',
  styleUrls: ['./platformitem.component.css']
})
export class PlatformitemComponent implements OnInit {

  @Input() item: Platform;
  @Input() i: number;
  editing: boolean = false;
  notediting: boolean = true;
  temp: Platform;
  constructor(private service: AppService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
}
initiateEdit() {
    this.editing = true;
    this.notediting = false;
    this.temp = JSON.parse(JSON.stringify(this.item));
}
  commitEdit() {
if (this.item.name == null) {
      this.openSnackBar("Please add Name.");
      return;
    }
this.editing = false;
    this.notediting = true;
    this.service.updatePlatform(this.item);
this.openSnackBar("Update success");  }
  cancelEdit() {
    this.editing = false;
    this.notediting = true;
    this.item = this.temp;
this.ngOnInit();
}
  openSnackBar(x) {
    this.snackBar.open(x, 'Dismiss', {
      duration: 2000
    })
  }
}
