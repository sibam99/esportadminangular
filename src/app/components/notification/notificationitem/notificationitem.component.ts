import { Component, OnInit, Input } from '@angular/core';
import { Notification } from '../notification';
import { AppService } from 'src/app/shared/services/app.service'; import { MatSnackBar } from '@angular/material/snack-bar';

import { Channel } from 'src/app/components/channel/channel';
@Component({
  selector: 'tr[app-notificationitem]',
  templateUrl: './notificationitem.component.html',
  styleUrls: ['./notificationitem.component.css']
})
export class NotificationitemComponent implements OnInit {

  @Input() item: Notification;
  @Input() i: number;
  editing: boolean = false;
  notediting: boolean = true;
  temp: Notification;
  constructor(private service: AppService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    if (this.item.channel != null) {
      this.loadChannel();
    }
  }
  loadChannel() {
    this.service.getChannelById(this.item.channel).subscribe(item => {
      this.channel = item;
    });
  }
  initiateEdit() {
    this.editing = true;
    this.notediting = false;
    this.temp = JSON.parse(JSON.stringify(this.item));
    this.loadAllChannels();
  }
  commitEdit() {
    if (this.item.title == null) {
      this.openSnackBar("Please add Title.");
      return;
    }
    if (this.item.message == null) {
      this.openSnackBar("Please add Message.");
      return;
    }
    if (this.channel != null) {
      this.item.channel = this.channel.id;
    }
    else {
      this.openSnackBar("Please add Channel");
      return;
    }
    this.editing = false;
    this.notediting = true;
    this.service.updateNotification(this.item);
    this.openSnackBar("Update success");
  }
  cancelEdit() {
    this.editing = false;
    this.notediting = true;
    this.item = this.temp;
    this.ngOnInit();
  }
  openSnackBar(x) {
    this.snackBar.open(x, 'Dismiss', {
      duration: 2000
    })
  }
  //code for adding a filter for channel.
  channel: Channel;
  channels: Channel[];
  loadingChannel = false;
  loadAllChannels() {
    this.loadingChannel = true;
    this.service.getAllChannels().subscribe(data => {
      this.channels = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data() as Channel
        };
      })
      this.channels.forEach(element => {
        if (this.item.channel == element.id) {
          this.channel = element
        }
      });
    });
  }
}
