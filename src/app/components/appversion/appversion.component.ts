import { Component, OnInit } from '@angular/core';
import { Appversion } from './appversion';
import { AngularFirestore } from '@angular/fire/firestore';
import { AppService } from 'src/app/shared/services/app.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';
import { FormControl } from '@angular/forms';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { Platform } from '../platform/platform';
import { Notification } from '../notification/notification';
import * as firebase from 'firebase';

@Component({
  selector: 'app-appversion',
  templateUrl: './appversion.component.html',
  styleUrls: ['./appversion.component.css']
})
export class AppversionComponent implements OnInit {
  creating: boolean;
  selitem: Appversion;
  showList: boolean = true;
  tableData: any;
  loading = true;
  count: number;
  constructor(private firestore: AngularFirestore, private service: AppService, private snackBar: MatSnackBar, private titleService: Title) { }

  ngOnInit(): void {
    this.setTitle('App Version Releases')
    this.loadItems();
    this.loadAllCounts();
  }
  //Save first document in snapshot of items received
  firstInResponse: any = [];

  //Save last document in snapshot of items received
  lastInResponse: any = [];

  //Keep the array of first document of previous pages
  prev_strt_at: any = [];

  //Maintain the count of clicks on Next Prev button
  pagination_clicked_count = 0;

  //Disable next and prev buttons
  disable_next: boolean = true;
  disable_prev: boolean = false;
  limit: number = 20;
  orderBy: string = 'createdat';
  dbPath: string = 'appversions';
  onCreate() {
    console.log('Creatting');
    this.selitem = new Appversion();
    this.showList = false;
    this.creating = true;
    this.loadAllPlatforms();
  }

  loadAllCounts() {
    this.firestore.collection(this.dbPath).snapshotChanges()
      .subscribe(response => {
        if (!response.length) {
          this.count = 0;
          this.dismissSpinner();
        }
        else if (response.length < this.limit) {
          this.count = 1;
        }
        else {
          this.count = Math.ceil(response.length / this.limit);
        }
        this.disable_next = (this.pagination_clicked_count + 1) == this.count;
      }, error => {
      });

  }

  loadItems() {
    this.firestore.collection(this.dbPath, ref => ref
      .limit(this.limit)
      .orderBy(this.orderBy, 'desc')
    ).snapshotChanges()
      .subscribe(response => {
        if (!response.length) {
          console.log("No Data Available");
          return false;
        }
        this.firstInResponse = response[0].payload.doc;
        this.lastInResponse = response[response.length - 1].payload.doc;

        this.tableData = [];
        for (let e of response) {
          this.tableData.push({
            id: e.payload.doc.id,
            ...e.payload.doc.data() as Appversion
          });

        }

        //Initialize values
        this.prev_strt_at = [];
        this.pagination_clicked_count = 0;
        this.disable_next = (this.pagination_clicked_count + 1) == this.count;
        this.disable_prev = false;

        //Push first item to use for Previous action
        this.push_prev_startAt(this.firstInResponse);
        this.loading = false;
      }, error => {
      });
  }
  datereleased = new FormControl(new Date());
  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }
  // Add item in Collection
  addItem() {
    if (this.selitem.versionion == null) {
      this.openSnackBar("Please add Version Number.");
      return;
    }
    if (this.platform == null) {
      this.openSnackBar("Please select Platform.");
      return;
    }
    
    if (this.platform != null) {
      this.selitem.platform = this.platform.id;
    }
    var datereleasedstr = moment(this.datereleased.value).set({ hour: 0, minute: 0, second: 0, millisecond: 0 }).valueOf();
    this.selitem.datereleased = datereleasedstr;
    if (this.selitem.message == null) {
      this.openSnackBar("Please add Notification Message.");
      return;
    }
    if (this.selitem.sendnotification == null) {
      this.openSnackBar("Please add Alert Users Now?.");
      return;
    }
    this.selitem.newwest=true;
    this.service.addAppversion(this.selitem).then((id) => {
      if(this.selitem.sendnotification)
      if (this.platform.name == 'Android') {
        const nots: Notification = {
          title:"New Version of TROP KE 🆕🆕🆕",
          channel: "leu1fegiHBVcEpiPO8Z9",
          createdat: firebase.firestore.FieldValue.serverTimestamp(),
          message: this.selitem.message,
          uniqueid:this.getRandomInt(999999999),
          id: "",
          docId:'',
          imageUrl:''
        };
        this.service.addNotification(nots);
      }
      if (this.platform.name == 'Ios') {
        const nots: Notification = {
          title: "New Version of TROP KE 🆕🆕🆕",
          channel: "U09H9xSLtecuQzIf5XGN",
          uniqueid:this.getRandomInt(999999999),
          createdat: firebase.firestore.FieldValue.serverTimestamp(),
          message: this.selitem.message,
          id: "",
          docId:'',
          imageUrl:''
        };
        this.service.addNotification(nots);
      }
    });
    this.openSnackBar("Record Added");
    this.creating = false;
    this.showList = true;
  }

  //Show previous set 
  prevPage() {
    this.showSpinner()
    this.disable_prev = true;
    this.firestore.collection(this.dbPath, ref => ref
      .orderBy(this.orderBy, 'desc')
      .startAt(this.get_prev_startAt())
      .endBefore(this.firstInResponse)
      .limit(this.limit)
    ).get()
      .subscribe(response => {
        this.firstInResponse = response.docs[0];
        this.lastInResponse = response.docs[response.docs.length - 1];

        this.tableData = [];
        for (let e of response.docs) {
          this.tableData.push({
            id: e.id,
            ...e.data() as Appversion
          });
        }

        //Maintaing page no.
        this.pagination_clicked_count--;

        //Pop not required value in array
        this.pop_prev_startAt(this.firstInResponse);

        //Enable buttons again
        this.disable_prev = false;
        this.disable_next = (this.pagination_clicked_count + 1) == this.count;
        this.disable_prev = false;
        this.dismissSpinner();
      });
  }
  showSpinner() {
    this.loading = true
  }
  dismissSpinner() {
    this.loading = false
  }
  nextPage() {
    this.showSpinner();
    this.disable_next = true;
    this.firestore.collection(this.dbPath, ref => ref
      .limit(this.limit)
      .orderBy(this.orderBy, 'desc')
      .startAfter(this.lastInResponse)
    ).get()
      .subscribe(response => {

        if (!response.docs.length) {
          this.disable_next = true;
          return;
        }

        this.firstInResponse = response.docs[0];

        this.lastInResponse = response.docs[response.docs.length - 1];
        this.tableData = [];
        for (let e of response.docs) {
          this.tableData.push({
            id: e.id,
            ...e.data() as Appversion
          });
        }


        this.pagination_clicked_count++;

        this.push_prev_startAt(this.firstInResponse);

        this.disable_next = (this.pagination_clicked_count + 1) == this.count;
        this.dismissSpinner();
      }, error => {
        this.dismissSpinner();
        this.disable_next = false;
      });
  }

  //Add document
  push_prev_startAt(prev_first_doc) {
    this.prev_strt_at.push(prev_first_doc);
  }

  //Remove not required document 
  pop_prev_startAt(prev_first_doc) {
    this.prev_strt_at.forEach(element => {
      if (prev_first_doc.data().id == element.data().id) {
        element = null;
      }
    });
  }

  //Return the Doc rem where previous page will startAt
  get_prev_startAt() {
    if (this.prev_strt_at.length > (this.pagination_clicked_count + 1))
      this.prev_strt_at.splice(this.prev_strt_at.length - 2, this.prev_strt_at.length - 1);
    return this.prev_strt_at[this.pagination_clicked_count - 1];
  }
  cancelCreate() {
    this.creating = false;
    this.showList = true;
  }

  openSnackBar(x) {
    this.dismissSpinner();
    this.snackBar.open(x, 'Dismiss', {
      duration: 2000
    })
  }
  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  platform: Platform;
  platforms: Platform[];
  filteredPlatforms: Observable<Platform[]>;
  loadingPlatform = false;
  searchplatform = new FormControl('');
  loadAllPlatforms() {
    this.loadingPlatform = true;
    this.service.getAllPlatforms().subscribe(data => {
      this.platforms = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data() as Platform
        };
      })

      if (this.platforms.length > 0) {
        this.platform = this.platforms[0]
      }
    });
  }
}
