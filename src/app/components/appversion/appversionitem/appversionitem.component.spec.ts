import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppversionitemComponent } from './appversionitem.component';

describe('AppversionitemComponent', () => {
  let component: AppversionitemComponent;
  let fixture: ComponentFixture<AppversionitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppversionitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppversionitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
