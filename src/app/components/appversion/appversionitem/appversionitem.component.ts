import { Component, OnInit, Input } from '@angular/core';
import { Appversion } from '../appversion';
import { AppService } from 'src/app/shared/services/app.service';import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl } from '@angular/forms';
import * as moment from 'moment';

import { Platform } from 'src/app/components/platform/platform';
@Component({
  selector: 'tr[app-appversionitem]',
  templateUrl: './appversionitem.component.html',
  styleUrls: ['./appversionitem.component.css']
})
export class AppversionitemComponent implements OnInit {

  @Input() item: Appversion;
  @Input() i: number;
  editing: boolean = false;
  notediting: boolean = true;
  temp: Appversion;
  constructor(private service: AppService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
if (this.item.platform != null) {
      this.loadPlatform();
    }
this.datereleased = new FormControl(moment(this.item.datereleased).toDate());
}
loadPlatform() {
    this.service.getPlatformById(this.item.platform).subscribe(item => {
      this.platform = item;
    });
  }
datereleased = new FormControl(new Date());
initiateEdit() {
    this.editing = true;
    this.notediting = false;
    this.temp = JSON.parse(JSON.stringify(this.item));
this.loadAllPlatforms();
}
  commitEdit() {
if (this.item.versionion == null) {
      this.openSnackBar("Please add Versionion.");
      return;
    }
if (this.item.datereleased == null) {
      this.openSnackBar("Please add Datereleased.");
      return;
    }
if (this.item.message == null) {
      this.openSnackBar("Please add Message.");
      return;
    }
if (this.item.sendnotification == null) {
      this.openSnackBar("Please add Sendnotification.");
      return;
    }
if (this.item.newwest == null) {
      this.openSnackBar("Please add Newwest.");
      return;
    }
if (this.platform != null) {
      this.item.platform = this.platform.id;
    }
    else{
      this.openSnackBar("Please add Platform");
      return;
    }
var datestr = moment(this.datereleased.value).set({hour:0,minute:0,second:0,millisecond:0}).valueOf()
    this.item.datereleased = datestr;
this.editing = false;
    this.notediting = true;
    this.service.updateAppversion(this.item);
this.openSnackBar("Update success");  }
  cancelEdit() {
    this.editing = false;
    this.notediting = true;
    this.item = this.temp;
this.ngOnInit();
}
  openSnackBar(x) {
    this.snackBar.open(x, 'Dismiss', {
      duration: 2000
    })
  }
//code for adding a filter for platform.
  platform: Platform;
  platforms: Platform[];
  loadingPlatform = false;
  loadAllPlatforms() {
    this.loadingPlatform = true;
    this.service.getAllPlatforms().subscribe(data => {
      this.platforms = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data() as Platform
        };
      })
      this.platforms.forEach(element => {
        if (this.item.platform==element.id) {
          this.platform=element
        }
      });
    });
  }
}
