import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { categoryviewComponent } from './categoryview.component';

describe('categoryviewComponent', () => {
  let component: categoryviewComponent;
  let fixture: ComponentFixture<categoryviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ categoryviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(categoryviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
