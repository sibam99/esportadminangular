import { Component, OnInit, Input } from '@angular/core';
import { Category } from '../category';
import { AppService } from 'src/app/shared/services/app.service'; 
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { AngularFireStorageReference, AngularFireUploadTask, AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { Match } from '../../matches/match';

@Component({
  selector: 'app-categoryview',
  templateUrl: './categoryview.component.html',
  styleUrls: ['./categoryview.component.css']
})
export class categoryviewComponent implements OnInit {

  @Input() id: string;
  item: Category;
  temp: Category;
  matches: Match[]
  editing: boolean; constructor(private service: AppService, private firestore: AngularFirestore, private storage: AngularFireStorage, private route: ActivatedRoute, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.getItem();
    });
  }
  getItem() {
    this.service.getCategoryById(this.id).subscribe(async item => {
      this.item = item;
      this.item.id = this.id;
      this.loadAllSubCategories();
    });
  }
  loadAllSubCategories() {
    this.firestore.collection('categories').doc(this.id).collection('matches').snapshotChanges().subscribe(response => {
      this.matches = [];
      for (let e of response) {
        this.matches.push({
          id: e.payload.doc.id,
          ...e.payload.doc.data() as Match
        });
      }
    }, error => {
    });
  }
  initiateEdit() {
    this.editing = true;
    this.temp = JSON.parse(JSON.stringify(this.item));
  }
  commitEdit() {
    if (this.item.categoryName == null || this.item.categoryName.length == 0) {
      this.openSnackBar("Please add Name.");
      return;
    }
    this.editing = false;
    this.service.updateCategory(this.item);
    this.openSnackBar("Update success");
  }
  cancelEdit() {
    this.editing = false;
    this.item = this.temp;
    this.ngOnInit();
  }
  openSnackBar(x) {
    this.snackBar.open(x, 'Dismiss', {
      duration: 2000
    })
  }

  uploading: boolean;

  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  uploadState: Observable<string>;
  uploadProgress: Observable<number>;
  downloadURL: Observable<string>;

  onFileChanged(event) {
    this.uploading = true;
    var n = this.service.randomString(40);
    const file = event.target.files[0];
    const filePath = `images/${n}`;
    const fileRef = this.storage.ref(filePath);
    this.task = this.storage.upload(`images/${n}`, file);
    this.uploadProgress = this.task.percentageChanges();
    this.task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.subscribe(url => {
            if (url) {
              this.item.categoryImage = url;
              this.uploading = false
            }
          });
        })
      )
      .subscribe(url => {
        if (url) {
          console.log(url);
        }
      });
  }
}
