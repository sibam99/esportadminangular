import { Component, OnInit, Input } from '@angular/core';
import { Category } from '../category';
import { AppService } from 'src/app/shared/services/app.service'; 
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'tr[app-categoryitem]',
  templateUrl: './categoryitem.component.html',
  styleUrls: ['./categoryitem.component.css']
})
export class CategoryitemComponent implements OnInit {

  @Input() item: Category;
  @Input() i: number;
  editing: boolean = false;
  notediting: boolean = true;
  temp: Category;
  constructor(private service: AppService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }
  initiateEdit() {
    this.editing = true;
    this.notediting = false;
    this.temp = JSON.parse(JSON.stringify(this.item));
  }
  commitEdit() {
    if (this.item.categoryName == null || this.item.categoryName.length == 0) {
      this.openSnackBar("Please add Name.");
      return;
    }
    this.editing = false;
    this.notediting = true;
    this.service.updateCategory(this.item);
    this.openSnackBar("Update success");
  }
  cancelEdit() {
    this.editing = false;
    this.notediting = true;
    this.item = this.temp;
    this.ngOnInit();
  }
  openSnackBar(x) {
    this.snackBar.open(x, 'Dismiss', {
      duration: 2000
    })
  }
}
