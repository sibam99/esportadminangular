import { Component, OnInit } from '@angular/core';
import { Channel } from './channel';
import { AngularFirestore } from '@angular/fire/firestore';
import { AppService } from 'src/app/shared/services/app.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.css']
})
export class ChannelComponent implements OnInit {
  creating: boolean;
  selitem: Channel;
  showList: boolean = true;
  tableData: any;
  loading=true;
  count: number;
  constructor(private firestore: AngularFirestore, private service: AppService, private snackBar: MatSnackBar,private titleService: Title) { }

  ngOnInit(): void {
    this.setTitle( 'Notifications Channels')
    this.loadItems();
    this.loadAllCounts();
  }
  //Save first document in snapshot of items received
  firstInResponse: any = [];

  //Save last document in snapshot of items received
  lastInResponse: any = [];

  //Keep the array of first document of previous pages
  prev_strt_at: any = [];

  //Maintain the count of clicks on Next Prev button
  pagination_clicked_count = 0;

  //Disable next and prev buttons
  disable_next: boolean = true;
  disable_prev: boolean = false;
  limit: number = 20;
  orderBy: string = 'createdat';
  dbPath: string = 'channels';
  onCreate() {
    console.log('Creatting');
    this.selitem = new Channel();
    this.showList = false;
    this.creating = true;
  }

  loadAllCounts() {
    this.firestore.collection(this.dbPath).snapshotChanges()
      .subscribe(response => {
        if (!response.length) {
          this.count = 0;
          this.dismissSpinner();
        }
        else if (response.length < this.limit) {
          this.count = 1;
        }
        else {
          this.count = Math.ceil(response.length / this.limit);
        }
        this.disable_next = (this.pagination_clicked_count+1)==this.count;
      }, error => {
      });

  }

  loadItems() {
    this.firestore.collection(this.dbPath, ref => ref
      .limit(this.limit)
      .orderBy(this.orderBy, 'desc')
    ).snapshotChanges()
      .subscribe(response => {
        if (!response.length) {
          console.log("No Data Available");
          return false;
        }
        this.firstInResponse = response[0].payload.doc;
        this.lastInResponse = response[response.length - 1].payload.doc;

        this.tableData = [];
        for (let e of response) {
          this.tableData.push({
            id: e.payload.doc.id,
            ...e.payload.doc.data() as Channel
          });

        }

        //Initialize values
        this.prev_strt_at = [];
        this.pagination_clicked_count = 0;
        this.disable_next = (this.pagination_clicked_count+1)==this.count;
        this.disable_prev = false;

        //Push first item to use for Previous action
        this.push_prev_startAt(this.firstInResponse);
        this.loading=false;
      }, error => {
      });
  }
// Add item in Collection
  addItem() {
if (this.selitem.name == null) {
      this.openSnackBar("Please add Channel Name.");
      return;
    }
this.service.addChannel(this.selitem);
this.openSnackBar("Record Added");
    this.creating = false;
    this.showList = true;
  }

  //Show previous set 
  prevPage() {
    this.showSpinner()
    this.disable_prev = true;
    this.firestore.collection(this.dbPath, ref => ref
      .orderBy(this.orderBy, 'desc')
      .startAt(this.get_prev_startAt())
      .endBefore(this.firstInResponse)
      .limit(this.limit)
    ).get()
      .subscribe(response => {
        this.firstInResponse = response.docs[0];
        this.lastInResponse = response.docs[response.docs.length - 1];

        this.tableData = [];
        for (let e of response.docs) {
          this.tableData.push({
            id: e.id,
            ...e.data() as Channel
          });
        }

        //Maintaing page no.
        this.pagination_clicked_count--;

        //Pop not required value in array
        this.pop_prev_startAt(this.firstInResponse);

        //Enable buttons again
        this.disable_prev = false;
        this.disable_next = (this.pagination_clicked_count+1)==this.count;
        this.disable_prev = false;
        this.dismissSpinner();
      });
  }
  showSpinner(){
    this.loading=true
  }
  dismissSpinner(){
    this.loading=false
  }
  nextPage() {
    this.showSpinner();
    this.disable_next = true;
    this.firestore.collection(this.dbPath, ref => ref
      .limit(this.limit)
      .orderBy(this.orderBy, 'desc')
      .startAfter(this.lastInResponse)
    ).get()
      .subscribe(response => {

        if (!response.docs.length) {
          this.disable_next = true;
          return;
        }

        this.firstInResponse = response.docs[0];

        this.lastInResponse = response.docs[response.docs.length - 1];
        this.tableData = [];
        for (let e of response.docs) {
          this.tableData.push({
            id: e.id,
            ...e.data() as Channel
          });
        }


        this.pagination_clicked_count++;

        this.push_prev_startAt(this.firstInResponse);

        this.disable_next = (this.pagination_clicked_count+1)==this.count;
        this.dismissSpinner();
      }, error => {
        this.dismissSpinner();
        this.disable_next = false;
      });
  }

  //Add document
  push_prev_startAt(prev_first_doc) {
    this.prev_strt_at.push(prev_first_doc);
  }

  //Remove not required document 
  pop_prev_startAt(prev_first_doc) {
    this.prev_strt_at.forEach(element => {
      if (prev_first_doc.data().id == element.data().id) {
        element = null;
      }
    });
  }

  //Return the Doc rem where previous page will startAt
  get_prev_startAt() {
    if (this.prev_strt_at.length > (this.pagination_clicked_count + 1))
      this.prev_strt_at.splice(this.prev_strt_at.length - 2, this.prev_strt_at.length - 1);
    return this.prev_strt_at[this.pagination_clicked_count - 1];
  }
  cancelCreate() {
    this.creating = false;
    this.showList = true;
  }

  openSnackBar(x) {
    this.dismissSpinner();
    this.snackBar.open(x, 'Dismiss', {
      duration: 2000
    })
  }
  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }

}
