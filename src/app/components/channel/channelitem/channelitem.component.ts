import { Component, OnInit, Input } from '@angular/core';
import { Channel } from '../channel';
import { AppService } from 'src/app/shared/services/app.service'; import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'tr[app-channelitem]',
  templateUrl: './channelitem.component.html',
  styleUrls: ['./channelitem.component.css']
})
export class ChannelitemComponent implements OnInit {

  @Input() item: Channel;
  @Input() i: number;
  editing: boolean = false;
  notediting: boolean = true;
  temp: Channel;
  constructor(private service: AppService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }
  initiateEdit() {
    this.editing = true;
    this.notediting = false;
    this.temp = JSON.parse(JSON.stringify(this.item));
  }
  commitEdit() {
    if (this.item.name == null) {
      this.openSnackBar("Please add Name.");
      return;
    }
    this.editing = false;
    this.notediting = true;
    this.service.updateChannel(this.item);
    this.openSnackBar("Update success");
  }
  cancelEdit() {
    this.editing = false;
    this.notediting = true;
    this.item = this.temp;
    this.ngOnInit();
  }
  openSnackBar(x) {
    this.snackBar.open(x, 'Dismiss', {
      duration: 2000
    })
  }
}
