import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromografixComponent } from './promografix.component';

describe('PromografixComponent', () => {
  let component: PromografixComponent;
  let fixture: ComponentFixture<PromografixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromografixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromografixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
