import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromografixitemComponent } from './promografixitem.component';

describe('PromografixitemComponent', () => {
  let component: PromografixitemComponent;
  let fixture: ComponentFixture<PromografixitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromografixitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromografixitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
