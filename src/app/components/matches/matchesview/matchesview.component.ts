import { Component, OnInit, Input } from '@angular/core';
import { Match } from '../match';
import { AppService } from 'src/app/shared/services/app.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl } from '@angular/forms';
import * as moment from 'moment';
import 'firebase/firestore';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-matchview',
  templateUrl: './matchesview.component.html',
  styleUrls: ['./matchesview.component.css']
})
export class MatchviewComponent implements OnInit {

  @Input() id: string;
  @Input() parentId: string;
  item: Match;
  temp: Match;
  editing: boolean; constructor(private firestore: AngularFirestore, private service: AppService, private route: ActivatedRoute, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.parentId = params.parent;
      this.getItem();
    });
    this.gameTime = new FormControl(moment(this.item.gameTime));
  }
  getItem() {
    console.log("Sub ID:::::::" + this.parentId);
    this.firestore.collection('categories').doc(this.parentId)
      .collection('matches').doc<Match>(this.id).valueChanges().subscribe(async item => {
        this.item = item;
        this.item.id = this.id;
        this.gameTime = new FormControl(moment(this.item.gameTime));
      });
  }
  gameTime = new FormControl(new Date());
  initiateEdit() {
    this.editing = true;
    this.gameTime = new FormControl(moment(this.item.gameTime));
    this.temp = JSON.parse(JSON.stringify(this.item));
  }
  commitEdit() {
    if (this.item.gameMap == null || this.item.gameMap.length == 0) {
      this.openSnackBar("Please add GameMap.");
      return;
    } 
    if (this.item.gameMode == null || this.item.gameMode.length == 0) {
      this.openSnackBar("Please add GameMode.");
      return; 
    }
   var datestr2 = moment(this.gameTime.value).valueOf()
    this.item.gameTime = datestr2;
    this.editing = false;
    this.service.updateMatch(this.item);
    this.openSnackBar("Update success");
  }
  cancelEdit() {
    this.editing = false;
    this.item = this.temp;
    this.ngOnInit();
    this.ngOnInit();
  }
  openSnackBar(x) {
    this.snackBar.open(x, 'Dismiss', {
      duration: 2000
    })
  }
}