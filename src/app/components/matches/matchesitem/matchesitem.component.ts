import { Component, OnInit, Input } from '@angular/core';
import { AppService } from 'src/app/shared/services/app.service'; 
import { MatSnackBar } from '@angular/material/snack-bar';
import { Match } from '../match';
@Component({
  selector: 'tr[app-matchesitem]',
  templateUrl: './matchesitem.component.html',
  styleUrls: ['./matchesitem.component.css']
})
export class MatchesitemComponent implements OnInit {

  @Input() item: Match;
  @Input() i: number;
  @Input() parentId: string;
  editing: boolean = false;
  notediting: boolean = true;
  temp: Match;
  constructor(private service: AppService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }
  openSnackBar(x) {
    this.snackBar.open(x, 'Dismiss', {
      duration: 2000
    })
  }
}
