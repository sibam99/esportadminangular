import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatchesitemComponent } from './matchesitem.component';
describe('sub_categoryitemComponent', () => {
  let component: MatchesitemComponent;
  let fixture: ComponentFixture<MatchesitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchesitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchesitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
