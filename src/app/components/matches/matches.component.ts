import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AppService } from 'src/app/shared/services/app.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';
import { AngularFireStorageReference, AngularFireUploadTask, AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import * as firebase from 'firebase';
import { finalize } from 'rxjs/operators';
import { Match } from './match';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-match',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.css']
})
export class MatchesComponent implements OnInit {
  @Input() parentId: string
  creating: boolean;
  selitem: Match;
  showList: boolean = true;
  tableData: any;
  loading = true;
  constructor(private storage: AngularFireStorage, private firestore: AngularFirestore, private service: AppService, private snackBar: MatSnackBar, private titleService: Title) { }

  ngOnInit(): void {
    this.setTitle('Matches')
    console.log('Parent ID:::' + this.parentId);
    this.loadItems();
  }
  dbPath: string = 'matches';
  onCreate() {
    console.log('Creatting');
    this.selitem = new Match();
    this.showList = false;
    this.creating = true;
  }
  gameTime = new FormControl(new Date());
  loadItems() {
    this.firestore.collection('categories').doc(this.parentId).collection(this.dbPath).snapshotChanges()
      .subscribe(response => {
        if (!response.length) {
          console.log("No Data Available");
          return false;
        }
        this.tableData = [];
        for (let e of response) {
          this.tableData.push({
            id: e.payload.doc.id,
            ...e.payload.doc.data() as Match
          });
        }
        this.loading = false;
      }, error => {
      });
  }
  // Add item in Collection
  addItem() {
    this.selitem.category = this.parentId
    var gameTimestr=moment(this.gameTime.value).valueOf();
    this.selitem.gameTime=gameTimestr;
    this.firestore.collection('categories').doc(this.parentId).collection(this.dbPath).doc(this.selitem.matchId).set({
      createdat: firebase.firestore.FieldValue.serverTimestamp(), ...this.selitem
    }) 
    this.showList = true;
    this.creating = false;
    this.openSnackBar('Sub Category Added');
  }

  showSpinner() {
    this.loading = true
  }
  dismissSpinner() {
    this.loading = false
  }
  cancelCreate() {
    this.creating = false;
    this.showList = true;
  }

  openSnackBar(x) {
    this.dismissSpinner();
    this.snackBar.open(x, 'Dismiss', {
      duration: 2000
    })
  }
  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  uploading: boolean;

  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  uploadState: Observable<string>;
  uploadProgress: Observable<number>;
  downloadURL: Observable<string>;

  onFileChanged(event) {
    this.uploading = true;
    var n = this.service.randomString(40);
    const file = event.target.files[0];
    const filePath = `images/${n}`;
    const fileRef = this.storage.ref(filePath);
    this.task = this.storage.upload(`images/${n}`, file);
    this.uploadProgress = this.task.percentageChanges();
    this.task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.subscribe(url => {
            if (url) {
              this.selitem.featuredImage = url;
              this.uploading = false
            }
          });
        })
      )
      .subscribe(url => {
        if (url) {
          console.log(url);
        }
      });
  }

}
