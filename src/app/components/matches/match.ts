

export class Match {
    id: string;
    createdat: any
    entryFee: number
    gameMap: string
    gameMode: string
    matchNumber: number
    maxRoom: number
    perKill: number
    featuredImage
    prizePool: number
    matchType:string
    category:string
gameTime:number
matchId:string
password:string
roomId:string
}
