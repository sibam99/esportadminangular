import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
// Reactive Form
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

// App routing modules
import { AppRoutingModule } from './shared/routing/app-routing.module';
import { IgxCalendarModule } from 'igniteui-angular';
// App components
import { AppComponent } from './app.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DefaultLayoutComponent } from './containers';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './components/verify-email/verify-email.component';

// Firebase services + enviorment module
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFireStorageModule } from '@angular/fire/storage';
import { environment } from '../environments/environment';
import { AngularFireDatabaseModule } from '@angular/fire/database';

// Auth service
import { AuthService } from "./shared/services/auth.service";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule
} from '@angular/material/button';
import {
  MatIconModule
} from '@angular/material/icon';
import {
  MatDialogModule
} from '@angular/material/dialog';
import {
  MatInputModule
} from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatCardModule } from '@angular/material/card';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { NotificationitemComponent } from './components/notification/notificationitem/notificationitem.component';
import { NotificationComponent } from './components/notification/notification.component';
import { UseritemComponent } from './components/user/useritem/useritem.component';
import { UserComponent } from './components/user/user.component';
import { MatBadgeModule } from '@angular/material/badge';
import { MatChipsModule } from '@angular/material/chips';
import { ModalModule, BsModalRef } from 'ngx-bootstrap/modal';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { MatListModule } from '@angular/material/list';
import { MatTabsModule } from '@angular/material/tabs';
import { PromografixComponent } from './components/promografix/promografix.component';
import { PromografixitemComponent } from './components/promografix/promografixitem/promografixitem.component';
import { SmallbannersComponent } from './components/smallbanners/smallbanners.component';
import { SmallbannersitemComponent } from './components/smallbanners/smallbannersitem/smallbannersitem.component';
import { ChannelComponent } from './components/channel/channel.component';
import { ChannelitemComponent } from './components/channel/channelitem/channelitem.component';
import { PlatformComponent } from './components/platform/platform.component';
import { PlatformitemComponent } from './components/platform/platformitem/platformitem.component';
import { AppversionComponent } from './components/appversion/appversion.component';
import { AppversionitemComponent } from './components/appversion/appversionitem/appversionitem.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { MatchesComponent } from './components/matches/matches.component';
import { MatchesitemComponent } from './components/matches/matchesitem/matchesitem.component';
import { MatchviewComponent } from './components/matches/matchesview/matchesview.component';
import { CategoryComponent } from './components/category/category.component';
import { CategoryitemComponent } from './components/category/categoryitem/categoryitem.component';
import { categoryviewComponent } from './components/category/categoryview/categoryview.component';
import { NgxMatDatetimePickerModule, NgxMatNativeDateModule, NgxMatTimepickerModule } from '@angular-material-components/datetime-picker';
const APP_CONTAINERS = [
  DefaultLayoutComponent
];
@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    SignUpComponent,
    DashboardComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent,
    DefaultLayoutComponent,
    UserComponent,
    NotificationComponent, NotificationitemComponent,
    UserComponent, UseritemComponent,ProfileComponent,
    PromografixComponent, PromografixitemComponent, 
    SmallbannersComponent, SmallbannersitemComponent, AppversionComponent, AppversionitemComponent,
    NotificationComponent, NotificationitemComponent,
    ChannelComponent, ChannelitemComponent,
    PlatformComponent, PlatformitemComponent,MatchesComponent,MatchesitemComponent,MatchviewComponent,CategoryComponent,CategoryitemComponent,categoryviewComponent
  ],
  imports: [ 
    MatButtonModule,NgxMatTimepickerModule,NgxMatNativeDateModule,
    BrowserModule, MatIconModule,   
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    BsDropdownModule.forRoot(), MatTabsModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule, MatChipsModule,
    ReactiveFormsModule,
    FormsModule, CommonModule, RichTextEditorAllModule, AngularFireStorageModule,
    MatDialogModule,
    ModalModule.forRoot(),
    MatInputModule,
    MatRadioModule, MatBadgeModule,
    MatSelectModule,
    NgxMatDatetimePickerModule,
    MatAutocompleteModule, MatCheckboxModule,
    HttpClientModule, BrowserAnimationsModule, HammerModule, IgxCalendarModule,
    MatSnackBarModule, OverlayModule, MatDatepickerModule, MatNativeDateModule, MatCardModule, FontAwesomeModule, MatTooltipModule, MatListModule
  ],
  providers: [AuthService, BsModalRef],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})

export class AppModule {
}
