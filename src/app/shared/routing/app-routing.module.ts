import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Required components for which route services to be activated
import { SignInComponent } from '../../components/sign-in/sign-in.component';
import { DashboardComponent } from '../../components/dashboard/dashboard.component';
// Import canActivate guard services
import { AuthGuard } from "../../shared/guard/auth.guard";
import { SecureInnerPagesGuard } from "../../shared/guard/secure-inner-pages.guard";
import { DefaultLayoutComponent } from 'src/app/containers';
import { NotificationComponent } from 'src/app/components/notification/notification.component';
import { UserComponent } from 'src/app/components/user/user.component';
import { ForgotPasswordComponent } from 'src/app/components/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from 'src/app/components/verify-email/verify-email.component';
import { AppversionComponent } from 'src/app/components/appversion/appversion.component';
import { ChannelComponent } from 'src/app/components/channel/channel.component';
import { PlatformComponent } from 'src/app/components/platform/platform.component';
import { PromografixComponent } from 'src/app/components/promografix/promografix.component';
import { SmallbannersComponent } from 'src/app/components/smallbanners/smallbanners.component';
import { ProfileComponent } from 'src/app/components/user/profile/profile.component';
import { CategoryComponent } from 'src/app/components/category/category.component';
import { categoryviewComponent } from 'src/app/components/category/categoryview/categoryview.component';
import { MatchviewComponent } from 'src/app/components/matches/matchesview/matchesview.component';


// Include route guard in routes array
const routes: Routes = [
  { path: '', redirectTo: '/sign-in', pathMatch: 'full' },
  { path: 'sign-in', component: SignInComponent, canActivate: [SecureInnerPagesGuard] },
  { path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [SecureInnerPagesGuard] },
  { path: 'verify-email-address', component: VerifyEmailComponent, canActivate: [SecureInnerPagesGuard] },
  {
    path: '', component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    }, children: [
      { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
      { path: 'notifications', component: NotificationComponent, canActivate: [AuthGuard] },
      { path: 'users', component: UserComponent, canActivate: [AuthGuard] },
      { path: 'categories', component: CategoryComponent, canActivate: [AuthGuard] },
      { path: 'categoryview/:id', component: categoryviewComponent, canActivate: [AuthGuard] },
      { path: 'matchview/:id/:parent', component: MatchviewComponent, canActivate: [AuthGuard] },
      { path: 'profile/:id', component: ProfileComponent, canActivate: [AuthGuard] },
      { path: 'appversions', component: AppversionComponent, canActivate: [AuthGuard] },
      { path: 'notifications', component: NotificationComponent, canActivate: [AuthGuard] },
      { path: 'channels', component: ChannelComponent, canActivate: [AuthGuard] },
      { path: 'platforms', component: PlatformComponent, canActivate: [AuthGuard] },
      { path: 'promografixes', component: PromografixComponent, canActivate: [AuthGuard] },
      { path: 'smallbannerses', component: SmallbannersComponent, canActivate: [AuthGuard] },
    ], canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }