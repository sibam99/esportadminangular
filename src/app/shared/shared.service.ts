import { Injectable } from '@angular/core';
import { DocumentReference } from '@angular/fire/firestore';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class SharedService {
ref:DocumentReference=null;
  private message = new BehaviorSubject<DocumentReference>(this.ref);
  sharedMessage = this.message.asObservable();

  constructor() { }

  nextMessage(message: DocumentReference) {
    this.message.next(message)
  }
  
}