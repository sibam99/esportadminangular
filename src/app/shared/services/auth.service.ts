import { Injectable, NgZone } from '@angular/core';
import { auth } from 'firebase/app';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router, ActivatedRoute } from "@angular/router";
import { User } from 'src/app/components/user/user';
import 'firebase/firestore';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  userData: any; // Save logged in user data

  constructor(
    public afs: AngularFirestore,   // Inject Firestore service
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,
    public ngZone: NgZone,
    private route: ActivatedRoute// NgZone service to remove outside scope warning
  ) {
    /* Saving user data in localstorage when 
    logged in and setting up null when logged out */
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
      }
    })
  }

  // Sign in with email/password
  async SignIn(email, password) {
    return await this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        this.ngZone.run(async () => {
          if (!result.user.emailVerified) {
            this.SendVerificationMail();
            return;
          } else {
            await this.afs.collection('users', ref => ref.where('email', '==', result.user.email)).get().forEach(async e => {
              if (e.docs.length > 0) {
                var item = {
                  id: e.docs[0].id,
                  ...e.docs[0].data() as User
                }
                if (item.role != undefined) {
                  if (item.role == 'admin') {
                    if (item.id != result.user.uid) {
                      await this.afs
                        .collection("users")
                        .doc(item.id)
                        .delete();
                      this.SetUserData(result.user);
                    }
                    localStorage.setItem('user', JSON.stringify(this.userData));
                    this.router.navigate(['/dashboard'], { relativeTo: this.route });
                  }
                  else {
                    window.alert('You are not authorised to access this system');
                    this.SignOut();
                  }
                } else {
                  window.alert('You are not authorised to access this system');
                  this.SignOut();
                }
              }

            });
          }
        });
      }).catch((error) => {
        window.alert(error.message)
      })
  }

  // Sign up with email/password
  SignUp(email, password) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then((result) => {
        /* Call the SendVerificaitonMail() function when new user sign 
        up and returns promise */
        this.SendVerificationMail();
        this.SetUserData(result.user);
      }).catch((error) => {
        window.alert(error.message)
      })
  }

  // Send email verfificaiton when new user sign up
  SendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
      .then(() => {
        this.router.navigate(['verify-email-address']);
      })
  }

  // Reset Forggot password
  ForgotPassword(passwordResetEmail) {
    return this.afAuth.auth.sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        window.alert('Password reset email sent, check your inbox.');
      }).catch((error) => {
        window.alert(error)
      })
  }

  // Returns true when user is looged in and email is verified
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null && user.emailVerified !== false) ? true : false;
  }

  // Sign in with Google
  GoogleAuth() {
    return this.AuthLogin(new auth.GoogleAuthProvider());
  } 

  // Auth logic to run auth providers
  async AuthLogin(provider) {
    console.log('Here');
    return await this.afAuth.auth.signInWithPopup(provider)
      .then((result) => {
        this.ngZone.run(async () => {
          console.log(result.user.email);
          await this.afs.collection('users').doc(result.user.uid).get().forEach(async e => {
            if (e.exists) {
              var item = {
                id: e.id,
                ...e.data() as User
              }
              e.ref.update({email:result.user.email});
              console.log('Here2');
              if (item.role != undefined) {
                if (item.role == 'admin') {
                  console.log('Here3');
                  localStorage.setItem('user', JSON.stringify(this.userData));
                  this.router.navigate(['/dashboard'], { relativeTo: this.route });
                }
                else {
                  console.log('Here4');
                  window.alert('You are not authorised to access this system');
                  this.SignOut();
                }
              } else {
                window.alert('You are not authorised to access this system');
                this.SignOut();
              }
            } else {
              this.SetUserData(result.user)
              window.alert('You are not authorised to access this system');
              this.SignOut();
            }
          });
        })
      }).catch((error) => {
        window.alert(error)
      })
  }

  /* Setting up user data when sign in with username/password, 
  sign up with username/password and sign in with social auth  
  provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
  SetUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    return userRef.set({
      uid: user.uid,
      email: user.email,
      firstname: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
      id: user.uid,
      createdat: firebase.firestore.FieldValue.serverTimestamp()
    })
  }

  // Sign out 
  SignOut() {
    return this.afAuth.auth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['sign-in']);
    })
  }
}