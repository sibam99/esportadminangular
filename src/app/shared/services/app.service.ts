import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'firebase/firestore';
import * as firebase from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/components/user/user';
import { Platform } from 'src/app/components/platform/platform';
import { Channel } from 'src/app/components/channel/channel';
import { Notification } from 'src/app/components/notification/notification';
import { Category } from 'src/app/components/category/category';
import { Match } from 'src/app/components/matches/match';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  addCategory(selitem: Category) {
    return this.dbf.collection('categories').add({ createdat: firebase.firestore.FieldValue.serverTimestamp(), ...selitem });
  }
  getCategoryById(id: string) {
    return this.dbf.doc<Category>('categories/' + id).valueChanges();
  }
  updateCategory(item: Category) {
    this.dbf
      .collection("categories")
      .doc(item.id)
      .set({ ...item }, { merge: true });
  }
  updateMatch(item: Match) {
    this.dbf.collection("categories")
      .doc(item.category)
      .collection("matches")
      .doc(item.id)
      .set({ ...item }, { merge: true });
  }   
  getUserById(user: string) {
    return this.dbf.doc<User>('users/' + user).valueChanges();
  }
  getAllUsers() {
    return this.dbf.collection('users').snapshotChanges();
  }
  keyAdded: string;
  constructor(private dbf: AngularFirestore, private http: HttpClient) {
  }

  public icon: string = 'https://firebasestorage.googleapis.com/v0/b/luteraa-esports.appspot.com/o/luteraa%20esports.png?alt=media&token=1c1e3a87-6640-4b1f-9b2b-f0e56bbc8243'

  public randomString(length) {
    const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
  }

  /* reply(replyInput: string, message: Message, user: User): string {
    var today = moment().format('DD MMM, YYYY  hh:mma');
    this.replyMessage = new MessageReply();
    this.replyMessage.dateSent = today;
    this.replyMessage.fromAdmin = 1;
    this.replyMessage.message = replyInput;
    this.replyMessage.messageId = message.key;
    this.repliesRef.push(this.replyMessage).then(_ => {
      console.log(_.key)
      const headers = { 'Authorization': 'key=AAAAEt0PG5Y:APA91bGmroUa2R05yUOhbMrcCsriirzWN0RSXoLLXAJw7ieBzeiPnAgHmWYF4Fz6qbl3K5N8Hkwo5svJiR-XAejiZAR32RXac0iVvgOtGlIhdxbGK6DuvhM2sd8xax4VI3JZpTP2jtws', 'Content-Type': 'application/json' }
      const body = {
        notification:
        {
          token: _.key,
          title: message.title, body: replyInput
        },
        data:
        {
          key: _.key,
          messageId: message.key,
          type: 'reply',
          click_action: "FLUTTER_NOTIFICATION_CLICK",
        },
        to:
          user.regId
      }
      console.log(body);
      this.http.post<any>('https://fcm.googleapis.com/fcm/send', body, { headers }).subscribe(data => {
        console.log(data);
      })
    });
    return ""
  } */


  addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
  }

  updateUser(item: import("../../components/user/user").User) {
    this.dbf
      .collection("users")
      .doc(item.id)
      .set({ ...item }, { merge: true });
  }
  addUser(selitem: import("../../components/user/user").User) {
    return this.dbf.collection('users').add({ createdat: firebase.firestore.FieldValue.serverTimestamp(), ...selitem });
  }
  addNotification(notification: import("../../components/notification/notification").Notification) {
    console.log('Here');
    this.getChannelById(notification.channel).subscribe((item) => {
      return this.dbf.collection('notifications').add({ ...notification }).then(_ => {
        console.log('Here');
        const headers = { 'Authorization': 'key=AAAAL8fHFDg:APA91bF44tNQ7nosBgH2t5VW2UktC1mTwS5qWd9xsHXEZoj8PhbBWBzpI7t3exmwteEtRoL27REvzNyYxnrhW5emZOa2uW93JM0f245Logc5254Dz1I1HiZH88agtWyz_Cngh7qFZtiX', 'Content-Type': 'application/json' }
        const body =
        {
          notification:
          {
            title: notification.title,
            body: notification.message
          }, priority: "high",
          data:
          {
            click_action: "FLUTTER_NOTIFICATION_CLICK",
            id: "1",
            status: "done",
            docId: notification.docId,
            imageUrl: notification.imageUrl
          },
          to: '/topics/' + item.name
        }
        console.log(body);
        this.http.post<any>('https://fcm.googleapis.com/fcm/send', body, { headers }).subscribe(data => {
        })
      });
    })

  }

  updateNotification(item: import("../../components/notification/notification").Notification) {
    this.dbf
      .collection("notifications")
      .doc(item.id)
      .set({ ...item }, { merge: true });
  } getAllNotifications() {
    return this.dbf.collection('notifications').snapshotChanges();
  }

  addAppversion(selitem: import("../../components/appversion/appversion").Appversion) {
    return this.dbf.collection('appversions').add({ createdat: firebase.firestore.FieldValue.serverTimestamp(), ...selitem });
  }

  updateAppversion(item: import("../../components/appversion/appversion").Appversion) {
    this.dbf
      .collection("appversions")
      .doc(item.id)
      .set({ ...item }, { merge: true });
  } getAllAppversions() {
    return this.dbf.collection('appversions').snapshotChanges();
  }
  getPlatformById(platform: string) {
    return this.dbf.doc<Platform>('platforms/' + platform).valueChanges();
  }

  getChannelById(channel: string) {
    return this.dbf.doc<Channel>('channels/' + channel).valueChanges();
  }
  addPlatform(selitem: import("../../components/platform/platform").Platform) {
    return this.dbf.collection('platforms').add({ createdat: firebase.firestore.FieldValue.serverTimestamp(), ...selitem });
  }

  updatePlatform(item: import("../../components/platform/platform").Platform) {
    this.dbf
      .collection("platforms")
      .doc(item.id)
      .set({ ...item }, { merge: true });
  } getAllPlatforms() {
    return this.dbf.collection('platforms').snapshotChanges();
  }
  addChannel(selitem: import("../../components/channel/channel").Channel) {
    return this.dbf.collection('channels').add({ createdat: firebase.firestore.FieldValue.serverTimestamp(), ...selitem });
  }

  updateChannel(item: import("../../components/channel/channel").Channel) {
    this.dbf
      .collection("channels")
      .doc(item.id)
      .set({ ...item }, { merge: true });
  } getAllChannels() {
    return this.dbf.collection('channels').snapshotChanges();
  }

  addPromografix(selitem: import("../../components/promografix/promografix").Promografix) {
    return this.dbf.collection('promografixes').add({ createdat: firebase.firestore.FieldValue.serverTimestamp(), ...selitem });
  }

  updatePromografix(item: import("../../components/promografix/promografix").Promografix) {
    this.dbf
      .collection("promografixes")
      .doc(item.id)
      .set({ ...item }, { merge: true });
  } getAllPromografixes() {
    return this.dbf.collection('promografixes').snapshotChanges();
  }
  addSmallbanners(selitem: import("../../components/smallbanners/smallbanners").Smallbanners) {
    return this.dbf.collection('smallbannerses').add({ createdat: firebase.firestore.FieldValue.serverTimestamp(), ...selitem });
  }

  updateSmallbanners(item: import("../../components/smallbanners/smallbanners").Smallbanners) {
    this.dbf
      .collection("smallbannerses")
      .doc(item.id)
      .set({ ...item }, { merge: true });
  } getAllSmallbannerses() {
    return this.dbf.collection('smallbannerses').snapshotChanges();
  }
  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }
  sendNotificationToUser(message: string, title: string, userRegId: string, ntype: string, userId: string) {
    var n = new Notification();
    n.message = message;
    n.title = title
    n.uniqueid = this.getRandomInt(999999999);
    n.channel = userRegId;
    this.addNotificationToUser(n, userRegId, userId, ntype);
  }

  addNotificationToUser(selitem: import("../../components/notification/notification").Notification, userRegId: string, userId: string, type: string) {
    this.sendNotification(userRegId, selitem.title, selitem.message, selitem.uniqueid, true, type);
    return this.dbf.collection('users').doc(userId).collection('notifications').add({ createdat: firebase.firestore.FieldValue.serverTimestamp(), ...selitem });
  }

  sendNotification(topic: string, title: string, message: string, id: number, touser: boolean, type: string) {
    console.log('Notification' + topic)
    const headers = { 'Authorization': "key=AAAAjyP0nro:APA91bE6x3jXFqgaPPmfOW1n-uNk3bJvfeeMh4KursrYXVeumkMFU-23siNdCcKS7KwQ86Tbe9-Rfmv7XTT_kixhzQG_btZ-9ekSCy9wV4tBWi58eAVu2As9srlY1egqJiYnxIE-hdc5", 'Content-Type': 'application/json' }
    const body =
    {
      notification:
      {
        title: title,
        body: message,
        id: id
      }, priority: "high",
      data:
      {
        click_action: "FLUTTER_NOTIFICATION_CLICK",
        id: id,
        status: "done",
        type: type
      },
      to: touser ? topic : ('/topics/' + topic)
    }
    console.log(body);
    this.http.post<any>('https://fcm.googleapis.com/fcm/send', body, { headers }).subscribe(data => {
      console.log(data);
    })
  }

  deleteItem(item: string, collection: string) {
    return this.dbf
      .collection(collection)
      .doc(item)
      .delete();
  }

  sendToAll(notification: import("../../components/notification/notification").Notification) {
    this.dbf.collection('users').get().forEach((element) => {
      element.docs.forEach((element) => {
        element.ref.collection('notifications').add({ createdat: firebase.firestore.FieldValue.serverTimestamp(), docId: 'allnots', ...notification });
      });
    })
  }

}